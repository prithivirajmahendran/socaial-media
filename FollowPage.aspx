﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="FollowPage.aspx.cs" Inherits="FollowPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    
    <div class="col-md-10" style="padding-top: 5px; padding-bottom: 5px">
        <h1>Find People</h1>
        <div runat="server" class="alert alert-warning" id="notificationbar" visible="false">
            <h5 id="warningmessage" runat="server"></h5>
        </div>
        <div class="input-group">
            <asp:TextBox ID="txtMailid" runat="server" class="form-control" placeholder="Search with email id..."></asp:TextBox>
            <span class="input-group-btn">
                <asp:Button ID="Button1" runat="server" Text="Go!" class="btn btn-default" type="button" OnClick="Button1_Click"></asp:Button>
            </span>
        </div>
        <div runat="server" id="useriddiv" hidden="hidden"></div>
       <asp:Repeater ID="Repeater1" runat="server" DataSourceID="SqlDataSource1" Visible="false">
        <ItemTemplate>
            <div>
                <div style="float: left" class="col-sm-2 col-md-2">
                    <div class="text-center" style="padding-top: 20px; padding-bottom: 20px;">
                        <a href="profile.html" class="rounded-image profile-image">
                            <img src="<%#Eval("imageurl")%>" style="width: 126px; height: 116px" /></a>
                    </div>

                </div>
                <div style="float: left" class="col-sm-8 col-md-8">
                    <article class="uou-block-7f">
                        <h3 style="font-family: Trajan"><a id="aaa" runat="server" href="#"><%#Eval("UserName")%></a></h3></br>
                        <asp:Button ID="btnFollow" class="btn btn-primary" runat="server" Text="Follow" OnClick="btnFollow_Click" />
                    </article>
                </div>
                <br></br>
                <br></br>
                <br></br>
                <br></br>
            </div>
        </ItemTemplate>
    </asp:Repeater>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select *,'Images/'+UserName+'.jpg' as imageurl from memberships m join users u on m.userid=u.userid where UPPER(m.Email)=@Emailid">
        <SelectParameters>
            <asp:ControlParameter ControlID="txtMailid" Name="Emailid" PropertyName="Text" />
        </SelectParameters>
</asp:SqlDataSource>
    </div>
</asp:Content>

