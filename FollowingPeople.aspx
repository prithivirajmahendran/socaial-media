﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="FollowingPeople.aspx.cs" Inherits="FollowingPeople" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="col-md-10" style="padding-top: 5px; padding-bottom: 5px">
        <h1>Connections</h1>
        <div runat="server" id="divuserid" hidden="hidden"></div>
        <asp:Repeater ID="Repeater1" runat="server" DataSourceID="SqlDataSource1">
            <ItemTemplate>
                <div>
                    <div style="float: left" class="col-sm-2 col-md-2">
                        <div class="text-center" style="padding-top: 20px; padding-bottom: 20px;">
                            <div runat="server" id="divuserid1" hidden="hidden"><%#Eval("FollowingUserId")%></div>
                            <a href="profile.html" class="rounded-image profile-image">
                                <img src="<%#Eval("imageurl")%>" style="width: 126px; height: 116px" /></a>
                        </div>

                    </div>
                    <div style="float: left" class="col-sm-8 col-md-8">
                        <article class="uou-block-7f">
                            <h3 style="font-family: Trajan"><a id="aaa" runat="server" href="#"><%#Eval("UserName")%></a></h3>
                            <asp:LinkButton class="btn btn-small btn-primary" ID="lnkUpdate" Text="Unfollow" runat="server" Visible="true" OnClick="OnUpdate" />
                            <br></br>
                        </article>
                    </div>
                    <br></br>
                    <br></br>
                    <br></br>
                    <br></br>
                </div>
            </ItemTemplate>
        </asp:Repeater>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="select U.UserName,'Images/'+U.UserName+'.jpg' as imageurl,f.FollowingUserId from Followers f join users u on f.FollowingUserId=u.UserId where f.UserId=@UserId">
            <SelectParameters>
                <asp:ControlParameter ControlID="divuserid" DefaultValue="" Name="UserId" PropertyName="InnerText" />
            </SelectParameters>
        </asp:SqlDataSource>
    </div>
</asp:Content>

