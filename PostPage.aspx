﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="~/PostPage.aspx.cs" Inherits="LoginPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div id="divuserid" style="visibility: hidden" runat="server"></div>
    <asp:Repeater ID="Repeater1" runat="server" DataSourceID="SqlDataSource1">
        <ItemTemplate>
            <div>
                <div style="float: left" class="col-sm-2 col-md-2">
                    <div class="text-center" style="padding-top: 20px; padding-bottom: 20px;">
                        <div runat="server" id="divmessageid" hidden="hidden"><%#Eval("MessageId")%></div>
                        <a href="profile.html" class="rounded-image profile-image">
                            <img src="<%#Eval("ImageUrl")%>" style="width: 126px; height: 116px" /></a>
                    </div>
                </div>
                <div style="float: left" class="col-sm-8 col-md-8">
                    <article class="uou-block-7f">
                        <h4 style="font-family: Trajan"><a href="#"><%#Eval("MessageTitle")%></a></h4>
                        <p style="font-family: Trajan"><%#Eval("Body")%></p><b><h5 style="font-family: Trajan">Likes: </b><%#Eval("Likes")%></h5>
                        <asp:LinkButton ID="btnLike" class="btn btn-primary" runat="server" Text="Like" OnClick="btnLike_Click" />
                    </article>
                </div>
                <br></br>
                <br></br>
                <br></br>
                <br></br>
            </div>
        </ItemTemplate>
    </asp:Repeater>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:DefaultConnection %>" SelectCommand="SELECT [Body], [ImageUrl], [MessageTitle],[Likes],[MessageId] FROM [Messages] [m] join [Followers] [f] on [m].UserId=[f].FollowingUserId where [f].UserId=@UserId ORDER BY [m].[Modified] DESC">
        <SelectParameters>
            <asp:ControlParameter ControlID="divuserid" DefaultValue="" Name="UserId" PropertyName="InnerText" />
        </SelectParameters>
    </asp:SqlDataSource>
</asp:Content>

