﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!User.Identity.IsAuthenticated)
        {
            Response.Redirect("LoginPage.aspx");
        }
    }

    protected void btnTweet_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtTweet.Text.Trim()) || string.IsNullOrEmpty(txtTweetMessage.Text.Trim()))
        {
            var stringvalue = "Please enter ";
            if (string.IsNullOrEmpty(txtTweet.Text.Trim()))
            {
                stringvalue += "Tweet Title ";
            }
            if (string.IsNullOrEmpty(txtTweetMessage.Text.Trim()))
            {
                stringvalue += "Tweet Message";
            }
            warningmessage.InnerHtml = stringvalue;
            notificationbar.Visible = true;
        }
        else {
            notificationbar.Visible = false;
            object id = System.Web.Security.Membership.GetUser().ProviderUserKey;
            var tweetTitle = txtTweet.Text;
            var tweetMessage = txtTweetMessage.Text;
            SqlConnection conn =  new SqlConnection("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\Database.mdf;Integrated Security=True;;MultipleActiveResultSets=True");
            SqlCommand insert = new SqlCommand("insert into Messages(UserId, MessageTitle, Body,ImageUrl,Created,Modified) values(@UserId, @MessageTitle, @Body,@ImageUrl,@Created,@Modified)", conn);
            insert.Parameters.AddWithValue("@UserId", System.Web.Security.Membership.GetUser().ProviderUserKey);
            insert.Parameters.AddWithValue("@MessageTitle", txtTweet.Text);
            insert.Parameters.AddWithValue("@Body", txtTweetMessage.Text);
            insert.Parameters.AddWithValue("@ImageUrl", "Images/"+User.Identity.Name+".jpg");
            insert.Parameters.AddWithValue("@Created", DateTime.Now);
            insert.Parameters.AddWithValue("@Modified", DateTime.Now);
            try
            {
                conn.Open();
                insert.ExecuteNonQuery();
                warningmessage.InnerHtml = "Tweet posted successfully..";
                notificationbar.Visible = true;
            }
            catch(Exception x)
            {
                warningmessage.InnerHtml =  "Error when saving on database";
                notificationbar.Visible = true;
                conn.Close();
            }
        }
    }
}