﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class FollowingPeople : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!User.Identity.IsAuthenticated)
        {
            Response.Redirect("LoginPage.aspx");
        }
        Object id = Membership.GetUser().ProviderUserKey;
        divuserid.InnerText = id.ToString();
    }
    protected void OnUpdate(object sender, EventArgs e)
    {
        //Find the reference of the Repeater Item.
        RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
        var followingid = ((System.Web.UI.HtmlControls.HtmlContainerControl)(item.FindControl("divuserid1"))).InnerHtml;
        string conn = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\Database.mdf;Integrated Security=True;;MultipleActiveResultSets=True";
        SqlConnection conn1 = new SqlConnection(conn);
        SqlCommand Delete = new SqlCommand("DELETE FROM Followers WHERE UserId = @UserId and FollowingUserId=@FollowingUserId; ", conn1);
        using (conn1)
        {
            using (Delete)
            {
                Delete.CommandType = CommandType.Text;
                Delete.Parameters.AddWithValue("@UserId", Membership.GetUser().ProviderUserKey.ToString());
                Delete.Parameters.AddWithValue("@FollowingUserId", followingid);
                Delete.Connection = conn1;
                conn1.Open();
                Delete.ExecuteNonQuery();
                conn1.Close();
            }
        }
        this.Repeater1.DataBind();
        //this.BindRepeater();
    }
}