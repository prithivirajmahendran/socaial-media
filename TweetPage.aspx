﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="TweetPage.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="col-md-10" style="padding-top:5px">
        <div runat="server" class="alert alert-warning" id="notificationbar" visible="false" >
            <h5 id="warningmessage" runat="server"></h5>
        </div>
        <h1>Tweet</h1>
        <div class="form-group">
            <label for="lbTweet">Tweet Title</label>
            <asp:TextBox class="form-control" ID="txtTweet" aria-describedby="emailHelp" placeholder="Enter tweet title.." runat="server"></asp:TextBox>
        </div>
        <div class="form-group">
            <label for="lbTweetMessage">Tweet Message</label>
            <asp:TextBox class="form-control" ID="txtTweetMessage" aria-describedby="emailHelp" placeholder="Enter tweet title.." runat="server"></asp:TextBox>
        </div>
        <div class="form-group">
            <asp:Button ID="btnTweet" class="btn btn-primary" runat="server" Text="Tweet" OnClick="btnTweet_Click" />
        </div>
    </div>
</asp:Content>

