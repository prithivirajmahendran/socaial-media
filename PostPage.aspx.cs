﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.AspNet.Identity;
using System.Web.Security;
using System.Data.SqlClient;
using System.Data;

public partial class LoginPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!User.Identity.IsAuthenticated)
        {
            Response.Redirect("LoginPage.aspx");   
        }
        Object id = Membership.GetUser().ProviderUserKey;
        divuserid.InnerText = id.ToString();
    }

    protected void btnFollow_Click(object sender, EventArgs e)
    {

    }

    protected void btnLike_Click(object sender, EventArgs e)
    {
        RepeaterItem item = (sender as LinkButton).Parent as RepeaterItem;
        var messageid = ((System.Web.UI.HtmlControls.HtmlContainerControl)(item.FindControl("divmessageid"))).InnerHtml;
        string conn = "Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\Database.mdf;Integrated Security=True;;MultipleActiveResultSets=True";
        SqlConnection conn1 = new SqlConnection(conn);
        SqlCommand Delete = new SqlCommand("Update messages set likes=likes+1 where MessageId=@messgeid; ", conn1);
        using (conn1)
        {
            using (Delete)
            {
                Delete.CommandType = CommandType.Text;
                Delete.Parameters.AddWithValue("@messgeid", messageid);
                Delete.Connection = conn1;
                conn1.Open();
                Delete.ExecuteNonQuery();
                conn1.Close();
            }
        }
        this.Repeater1.DataBind();
    }
}