﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class FollowPage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtMailid.Text.Trim()))
        {
            var stringvalue = "";
            if (string.IsNullOrEmpty(txtMailid.Text.Trim()))
            {
                stringvalue = "Please enter Mail id";
            }

            warningmessage.InnerHtml = stringvalue;
            notificationbar.Visible = true;
        }
        else
        {
            var emailid = txtMailid.Text.Trim().ToUpper();
            string constr = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            using (SqlConnection con = new SqlConnection(constr))
            {
                using (SqlCommand cmd = new SqlCommand("select * from memberships m join users u on m.userid=u.userid where UPPER(m.Email)=@Emailid", con))
                {
                    using (SqlDataAdapter sda = new SqlDataAdapter())
                    {
                        cmd.Connection = con;
                        cmd.Parameters.AddWithValue("@Emailid", emailid);
                        sda.SelectCommand = cmd;
                        using (DataTable dt = new DataTable())
                        {
                            sda.Fill(dt);
                            if (dt.Rows.Count > 0)
                            {
                                Repeater1.Visible = true;
                                useriddiv.InnerText = (dt.Rows[0]).ItemArray[0].ToString();
                            }
                            else
                            {
                                var stringvalue = "Please enter the correct mail id";
                                warningmessage.InnerHtml = stringvalue;
                                notificationbar.Visible = true;
                            }
                        }
                    }
                }
            }
        }
    }

    protected void btnFollow_Click(object sender, EventArgs e)
    {
        var followuserid = useriddiv.InnerText;
        SqlConnection conn = new SqlConnection("Data Source=(LocalDB)\\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\\Database.mdf;Integrated Security=True;;MultipleActiveResultSets=True");
        SqlCommand insert = new SqlCommand("insert into Followers(UserId, FollowingUserId) values (@UserId, @FollowUserId)", conn);
        insert.Parameters.AddWithValue("@UserId", System.Web.Security.Membership.GetUser().ProviderUserKey);
        insert.Parameters.AddWithValue("@FollowUserId", useriddiv.InnerText);
        try
        {
            conn.Open();
            insert.ExecuteNonQuery();
            warningmessage.InnerHtml = "You are now following this person";
            notificationbar.Visible = true;
        }
        catch (Exception x)
        {
            warningmessage.InnerHtml = "Error when saving on database";
            notificationbar.Visible = true;
            conn.Close();
        }
    }
}